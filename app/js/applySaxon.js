'use strict'
"use strict"

function applySaxon() {
   SaxonJS.transform({
      stylesheetLocation: "../app/Saxon/styling.sef.xml",
      initialTemplate:"main"
   })
}

function getChildren() {
    if(document.getElementById('the-return').innerHTML !== " ") {
        applySaxon();
    }
}

function displayButton(className) {
    var showButton = document.getElementsByClassName('btn btn-primary')[0];
    if(className == 'show') {
        showButton.classList.remove('hidden')
    } else {
        showButton.classList.add('hidden')
    }
}
