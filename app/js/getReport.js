'use strict'
"use strict"

function initCurl(samling) {
  let samlingen = encodeURI(soketermer[1]);
  let datatoPass = 'samling=' + samlingen;

  $.ajax({
    type: 'POST',
    url: "../curl.php",
    data: datatoPass,
    beforeSend: function() {
      //Adds the spinner to the HTML while the request is not finished
      let loader = document.getElementById('load');
      loader.setAttribute("id", "loader")
    },
    success: function(xmlResponse) {
      console.log(xmlResponse)
      $("#the-return").text(xmlResponse);
      //Hacky way to remove the spinner when the request is finished
      loader.setAttribute("id", "loaded")
      applySaxon();
      displayButton('show');
    }
  });
};
