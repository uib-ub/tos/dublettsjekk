'use strict'
"use strict"

var doubletcheck = angular.module('doubletcheck', []);

angular.module('myModule', ['ui.bootstrap']);


var avdelinger = [
    {navn: 'Bibliotek for humaniora', kortform: 'UBBHF'},
    {navn: 'Bibliotek for juridiske fag', kortform: 'UBBJUR'},
    {navn: 'Bibliotek for kunst og design', kortform: 'UBBKD'},
    {navn: 'Bibliotek for medisin', kortform: 'UBBMED'},
    {navn: 'Bibliotek for psykolog, utdanning og helse', kortform: 'UBBPS'},
    {navn: 'Bibliotek for matematisk-naturvitenskapelige fag', kortform: 'UBBRB'},
    {navn: 'Bibliotek for samfunnsvitenskap og musikk', kortform: 'UBBSV'},
];



    var collections = [];
    //var collectionObject = []
    var object1 = {};
/*
getCollections();

//Funksjon for å hente samlinger
function getCollections() {
    for (var i = 0; i < avdelinger.length; i++) {
      var avdelingnavn = avdelinger[i].kortform
      var pathToFile = 'http://158.39.77.93/collections/'+avdelingnavn+'.txt'
      populateCollectionsDropdown(pathToFile)
    }
}


function getCollectionsObject(collectionFromXhr) {
    console.log(collectionFromXhr)
}
*/

var soketermer = [];

//COLLECTIONS
var samlingerHF = [{navn: 'UBBHF'}, {navn: 'UBBHF Avh'}, {navn: 'UBBHF Bryggen'}, {navn: 'UBBHF CD-ROM Z'}, {navn: 'UBBHF Etnogra'}, {navn: 'UBBHF HmusB'}, {navn: 'UBBHF Håndbibl'}, {navn: 'UBBHF Kvin'}, {navn: 'UBBHF Ls'}, {navn: 'UBBHF Mag'}, {navn: 'UBBHF Mag NS'}, {navn: 'UBBHF Mag Z'}, {navn: 'UBBHF Mag fZ'}, {navn: 'UBBHF Mag qZ'}, {navn: 'UBBHF Ref'}, {navn: 'UBBHF Ref Z'}, {navn: 'UBBHF Svt'}, {navn: 'UBBHF Z'}, {navn: 'UBBHF fZ'}, {navn: 'UBBHFINST'}, {navn: 'UBBHFINST Arab'}, {navn: 'UBBHFINST Ark'}, {navn: 'UBBHFINST Ark Z'}, {navn: 'UBBHFINST Eng'}, {navn: 'UBBHFINST Etno'}, {navn: 'UBBHFINST Etno Z'}, {navn: 'UBBHFINST Fil'}, {navn: 'UBBHFINST Fon'}, {navn: 'UBBHFINST Fon Z'}, {navn: 'UBBHFINST Germ'}, {navn: 'UBBHFINST Germ Z'}, {navn: 'UBBHFINST Hist'}, {navn: 'UBBHFINST Klas'}, {navn: 'UBBHFINST Kontor'}, {navn: 'UBBHFINST Kunst'}, {navn: 'UBBHFINST Kunst Z'}, {navn: 'UBBHFINST Litt'}, {navn: 'UBBHFINST Nord'}, {navn: 'UBBHFINST Nord Z'}, {navn: 'UBBHFINST Ref'}, {navn: 'UBBHFINST Rel'}, {navn: 'UBBHFINST Rel Z'}, {navn: 'UBBHFINST Rom'}, {navn: 'UBBHFINST Rom Z'}, {navn: 'UBBHFINST Russ'}, {navn: 'UBBHFINST Russ Z'}, {navn: 'UBBHFINST Teat'}, {navn: 'UBBHFINST Z'}];

var samlingerJUR = [{navn: 'UBBJUR'}, {navn: 'UBBJUR Før 1850. Ikke utlån'}, {navn: 'UBBJUR Kildesamling. Ikke utlån'}, {navn: 'UBBJUR Magasin'}, {navn: 'UBBJUR Pensum. Ikke utlån'}, {navn: 'UBBJUR Pensum. Korttidslån'}, {navn: 'UBBJUR Referansesamling. Ikke utlån'}, {navn: 'UBBJUR Skranken. Ikke utlån'}, {navn: 'UBBJUR Tidsskrift'}, {navn: 'UBBJUR Tidsskrift. Ikke utlån'}, {navn: 'UBBJUR Utstilling'}];

var samlingerKD = [{navn: 'UBBKD'}, {navn: 'UBBKD Artb'}, {navn: 'UBBKD Artz'}, {navn: 'UBBKD Bb'}, {navn: 'UBBKD CD'}, {navn: 'UBBKD Fjernlån'}, {navn: 'UBBKD K-samling'}, {navn: 'UBBKD Kmag'}, {navn: 'UBBKD Kompakt'}, {navn: 'UBBKD Kontor'}, {navn: 'UBBKD Lukket samling'}, {navn: 'UBBKD Mag'}, {navn: 'UBBKD Ref'}, {navn: 'UBBKD Tidsskrift'}, {navn: 'UBBKD Video'}, {navn: 'UBBKD XL-samling'}];

var samlingerMED = [{navn: 'UBBMED'}, {navn: 'UBBMED Film'}, {navn: 'UBBMED Librar'}, {navn: 'UBBMED Mag'}, {navn: 'UBBMED Ref'}, {navn: 'UBBMED Skranken'}, {navn: 'UBBMED Utstilling'}, {navn: 'UBBMED Z'}];

var samlingerPS = [{navn: 'UBBPS'}, {navn: 'UBBPS DVD'}, {navn: 'UBBPS Hfoppg. (Ikke utlån)'}, {navn: 'UBBPS Intern'}, {navn: 'UBBPS Kjellermag'}, {navn: 'UBBPS Lydbok'}, {navn: 'UBBPS P-samling Kjellermag'}, {navn: 'UBBPS Pensum (Ikke utlån)'}, {navn: 'UBBPS Ref (Ikke utlån)'}, {navn: 'UBBPS Ref Z'}, {navn: 'UBBPS Skriving'}, {navn: 'UBBPS Spes.saml.'}, {navn: 'UBBPS Test (Ikke utlån)'}, {navn: 'UBBPS Video'}, {navn: 'UBBPS Z (Ikke utlån)'}, {navn: 'UBBPS Z Kjellermag'}, {navn: 'UBBPS fZ'}];

var samlingerRB = [{navn: 'UBBRB'}, {navn: 'UBBRB Arbor'}, {navn: 'UBBRB ArborRef'}, {navn: 'UBBRB Barn'}, {navn: 'UBBRB Bio'}, {navn: 'UBBRB BioRef'}, {navn: 'UBBRB Bothag'}, {navn: 'UBBRB Botins'}, {navn: 'UBBRB BotinsRef'}, {navn: 'UBBRB Box'}, {navn: 'UBBRB CD-ROM'}, {navn: 'UBBRB Diskett Z'}, {navn: 'UBBRB Diss1'}, {navn: 'UBBRB Diss2'}, {navn: 'UBBRB Folio'}, {navn: 'UBBRB Geofy'}, {navn: 'UBBRB Hovedopp'}, {navn: 'UBBRB Ifm'}, {navn: 'UBBRB IfmNOFI'}, {navn: 'UBBRB Kart'}, {navn: 'UBBRB Kjem'}, {navn: 'UBBRB KjemRef'}, {navn: 'UBBRB KjemRefZ'}, {navn: 'UBBRB Kompaktus'}, {navn: 'UBBRB Korttid'}, {navn: 'UBBRB Librar'}, {navn: 'UBBRB Mag'}, {navn: 'UBBRB MagA'}, {navn: 'UBBRB Mikrof'}, {navn: 'UBBRB MikrofRef'}, {navn: 'UBBRB MikrofZ'}, {navn: 'UBBRB MsRb'}, {navn: 'UBBRB N box'}, {navn: 'UBBRB Pauserom'}, {navn: 'UBBRB Pensum'}, {navn: 'UBBRB PopVit'}, {navn: 'UBBRB Prog'}, {navn: 'UBBRB Rapp1'}, {navn: 'UBBRB Rapp2'}, {navn: 'UBBRB RbFys'}, {navn: 'UBBRB RbFysRef'}, {navn: 'UBBRB RbFysZ'}, {navn: 'UBBRB RbGeofy'}, {navn: 'UBBRB RbImbio'}, {navn: 'UBBRB RbKjem'}, {navn: 'UBBRB RbMat'}, {navn: 'UBBRB RbZooll'}, {navn: 'UBBRB RbZoolm'}, {navn: 'UBBRB Realia'}, {navn: 'UBBRB Ref'}, {navn: 'UBBRB RefZ'}, {navn: 'UBBRB Skranken'}, {navn: 'UBBRB Z'}];

var samlingerSV = [{navn: 'UBBSV'}, {navn: 'UBBSV Atlas'}, {navn: 'UBBSV Box'}, {navn: 'UBBSV Film'}, {navn: 'UBBSV Finis Afr.'}, {navn: 'UBBSV Ikke utlån'}, {navn: 'UBBSV Kontor'}, {navn: 'UBBSV Korttid'}, {navn: 'UBBSV MsEx'}, {navn: 'UBBSV Oppst EHT'}, {navn: 'UBBSV Ref'}, {navn: 'UBBSV Ref Box'}, {navn: 'UBBSV Ref2'}, {navn: 'UBBSV Reise'}, {navn: 'UBBSV SVinfo'}, {navn: 'UBBSV Sendt til NBR 2017'}, {navn: 'UBBSV Skranken'}, {navn: 'UBBSV Z'}, {navn: 'UBBSV film'}, {navn: 'UBBSV folio'}];



//Controller for drop-downs
doubletcheck.controller("myCtrl", ['$scope', function($scope) {

    $scope.avdelingene = avdelinger;

    $scope.selectedAvdeling = $scope.avdelingene;

    $scope.samlingerHF = samlingerHF;

    $scope.selectSamlingHF = $scope.samlingerHF;

    $scope.samlingerJUR = samlingerJUR;

    $scope.selectSamlingJUR = $scope.samlingerJUR;

    $scope.samlingerKD = samlingerKD;

    $scope.selectSamlingKD = $scope.samlingerKD;

    $scope.samlingerMED = samlingerMED;

    $scope.selectSamlingMED = $scope.samlingerMED;

    $scope.samlingerPSYK = samlingerPS;

    $scope.selectSamlingPSYK = $scope.samlingerPSYK;

    $scope.samlingerRB = samlingerRB;

    $scope.selectSamlingRB = $scope.samlingerRB;

    $scope.samlingerSOG = samlingerSV;

    $scope.selectSamlingSOG = $scope.samlingerSOG;

    //If both department and collection is chosen - fire of the function responsible for getting the analytics report
    $scope.changedValue = function(term) {
        soketermer.push(term.navn)
        if(soketermer.length == 2) {
        initCurl(soketermer)
        }
    }


    function clearScope() {
      $scope.selectedAvdeling = null;
      soketermer = [];
      clearHTML();
    }

    function clearHTML() {
      $("#the-return").empty()
      var loaderDone = document.getElementById('loaded');
      loaderDone.setAttribute("id", "load");
      displayButton()

    }

    $scope.reloadSearch = function() {
      clearScope();
    }

}

]);
