<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
    exclude-result-prefixes="xs" xmlns:ubb="urn:schemas-microsoft-com:xml-analysis:rowset"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:saw-sql="urn:saw-sql" version="3.0">
    <xsl:variable name="xml-import"
        select="parse-xml(ixsl:page()/descendant::div[@id = 'the-return'])" as="document-node()"/>

    <xsl:template match="/" name="main">
        <xsl:call-template name="show-all"/>
    </xsl:template>
    <xsl:template name="show-all">

        <xsl:result-document href="#the-return" method="ixsl:replace-content">
            <xsl:variable name="pathToRows"
                select="$xml-import/report/QueryResult/ResultXml/ubb:rowset/ubb:Row"/>
            <xsl:text expand-text="1"/>
            <xsl:variable name="bodyID" select="empty"/>
            <xsl:variable name="aggregator">
                <xsl:value-of select="count($pathToRows)"/>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="$aggregator != 0">
                    <html>
                        <body>
                            <!-- Aggregator that displays no. of lines in report -->
                            <div id="aggregator">
                                <h3>
                                    <xsl:value-of
                                        select="concat('Antall linjer funnet: ', $aggregator)"/>
                                </h3>
                                <h5>Klikk på overskriften i kolonnene for å sortere</h5>
                            </div>
                            <!-- /Aggregator -->

                            <table border="1" id="myTable">
                                <thead>
                                    <tr bgcolor="#00769e" style="color:#ffffff">
                                        <th data-type="number">Antall eksemplarer</th>
                                        <th>Forfatter</th>
                                        <th>Tittel</th>
                                        <th>Plassering</th>
                                        <th data-type="number">Antall lån (fra nov. 2015)</th>
                                        <th>Utgivelsesår</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$pathToRows">
                                        <tr>
                                            <td>
                                                <xsl:value-of select="ubb:Column9"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="ubb:Column1"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="ubb:Column3"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="ubb:Column4"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="ubb:Column10"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="ubb:Column2"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                        </body>
                    </html>
                </xsl:when>
                <xsl:otherwise>
                    <html>
                        <body>
                            <div id="empty-result">
                                <h1 id="empty-result-headline">Null dubletter funnet</h1>
                                <p>
                                    <img src="/img/empty-set.svg" class="center"/>
                                </p>
                            </div>
                        </body>
                    </html>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:result-document>
    </xsl:template>

    <!-- SORTING -->
    <xsl:template match="th" mode="ixsl:onclick">
        <xsl:variable name="colNr" as="xs:integer" select="count(preceding-sibling::th) + 1"/>
        <xsl:apply-templates select="ancestor::table[1]" mode="sort">
            <xsl:with-param name="colNr" select="$colNr"/>
            <xsl:with-param name="dataType"
                select="
                    if (@data-type = 'number') then
                        'number'
                    else
                        'text'"/>
            <xsl:with-param name="ascending" select="not(../../@data-order = $colNr)"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="table" mode="sort">
        <xsl:param name="colNr" as="xs:integer" required="yes"/>
        <xsl:param name="dataType" as="xs:string" required="yes"/>
        <xsl:param name="ascending" as="xs:boolean" required="yes"/>
        <xsl:result-document href="#myTable" method="ixsl:replace-content">

            <xsl:assert test="thead/tr"/>
            <xsl:if test="not(thead/tr)">
                <xsl:message terminate="yes" select="'finner ikke xpath thead/tr'"/>
            </xsl:if>
            <thead data-order="{if ($ascending) then $colNr else -$colNr}">
                <xsl:copy-of select="thead/tr"/>
            </thead>
            <tbody>
                <xsl:perform-sort select="tbody/tr">
                    <xsl:sort select="td[$colNr]" data-type="{$dataType}"
                        order="{if ($ascending) then 'ascending' else 'descending'}"/>
                </xsl:perform-sort>
            </tbody>
        </xsl:result-document>
    </xsl:template>
    <!-- /SORTING -->
</xsl:stylesheet>
